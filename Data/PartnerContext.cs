using Microsoft.EntityFrameworkCore;
using PartnersApi.Models;

namespace PartnersApi.Data
{
    public class PartnerContext : DbContext
    {
        public PartnerContext(DbContextOptions<PartnerContext> opt) : base(opt){}
        public DbSet<Partner> Partners { get; set; }
    }
}