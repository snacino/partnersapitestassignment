using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Net.Http.Headers;
using Microsoft.EntityFrameworkCore;
using PartnersApi.Models;
namespace PartnersApi.Data
{
    public class PartnerRepo : IPartnerRepo
    {
        private readonly PartnerContext _context;
        public PartnerRepo(PartnerContext context)
        {
            _context = context;
        }
        public IEnumerable<Partner> GetAllPartners()
        {
            return _context.Partners.ToList();
        }
        public Partner GetPatnerById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Partner> CreateNewPartners(IEnumerable<Partner> partners)
        {
            foreach (Partner partner in partners)
            {
                _context.Partners.Add(partner);
            }
            _context.SaveChanges();

            // Return all partners in db after insert
            var allPartners = GetAllPartners();
            return allPartners;
        }

        public void DeleteAllParnters()
        {
            _context.Database.ExecuteSqlRaw("TRUNCATE TABLE [Partners]");
            _context.SaveChanges();
        }

        public void DeleteParnter(long Pid)
        {   
            Console.WriteLine(Pid);
            var partnerToDelete = _context.Partners.Where(partner => partner.Pid == Pid).FirstOrDefault();
            if (partnerToDelete != null)
            {
                _context.Partners.Remove(partnerToDelete);
                _context.SaveChanges();
            } else {
                Console.WriteLine("Could not get partner with give Pid.");
                throw new EntityException("Could not delete Partner with given Pid");
            }
        }
    }
}