using System.Collections.Generic;
using PartnersApi.Models;

namespace PartnersApi.Data
{
    public interface IPartnerRepo
    {
         Partner GetPatnerById(int id);
        IEnumerable<Partner> GetAllPartners();
        IEnumerable<Partner> CreateNewPartners(IEnumerable<Partner> partners);
        void DeleteAllParnters();
        void DeleteParnter(long pid);
    }
}