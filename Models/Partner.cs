using System.ComponentModel.DataAnnotations;

namespace PartnersApi.Models
{
    public class Partner
    {
        [Key]
        public int Id { get; set; }
        [Required]
        // TODO: Use this as PK
        public long Pid { get; set; }
        [Required]
        public string Name { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }
    }
}