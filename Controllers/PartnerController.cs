using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using PartnersApi.Models;
using PartnersApi.Data;
using System;

using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Cors;

namespace PartnersApi.Controllers
{
    [Route("api/partner")]
    [ApiController]
    public class PartnerController : Controller
    {

        private readonly IPartnerRepo _repository;
        public PartnerController(IPartnerRepo repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Partner>> GetAllPartners()
        {
            return Ok(_repository.GetAllPartners());
        }
        
        [HttpPost]
        public ActionResult<IEnumerable<Partner>> CreateNewPartner(IEnumerable<Partner> partners)
        {
            Console.WriteLine("Inserting partners");
            _repository.CreateNewPartners(partners);
            return Ok(partners);
        }

        [HttpDelete("truncate")]
        public ActionResult<IEnumerable<Partner>> DeletePartners()
        {
            //Delete all records from partners
            _repository.DeleteAllParnters();
            return Ok();
        }

        [HttpDelete("{Pid}")]
        public ActionResult<String> DeletePartner(long Pid)
        {
            //Refator this to be delete
            try
            {
                _repository.DeleteParnter(Pid);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return NoContent();
        }
    }
}